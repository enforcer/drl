# -*- coding: utf-8 -*-
from distutils.core import setup

setup(
    name='drl',
    version='1.0',
    packages=['drl'],
    package_dir={'drl': 'src/drl'},
    url='',
    license='',
    author='Sebastian Buczyński',
    author_email='poczta@enforcer.pl',
    description='',
    install_requires=[
        'redis==2.10.1',
    ]
)
