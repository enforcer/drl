import unittest
import drl


class MutexTestSuite(unittest.TestCase):
    def setUp(self):
        drl._r_con.flushdb()
        self.mutex = drl.Mutex('some_mutex')

    def test_acquire_mutex(self):
        result = self.mutex.acquire()
        self.assertTrue(result)

    def test_acquire_mutex_twice(self):
        result_first = self.mutex.acquire()
        self.assertTrue(result_first)

        self.assertRaises(RuntimeError, self.mutex.acquire)

    def test_release_not_acquired(self):
        self.assertRaises(RuntimeError, self.mutex.release)

    def test_release_acquired_mutex(self):
        self.mutex.acquire()
        self.assertTrue(self.mutex.release())


class SemaphoreTestSuite(unittest.TestCase):
    SEMAPHORE_INIT_COUNT = 3

    def setUp(self):
        drl._r_con.flushdb()
        self.semaphore = drl.Semaphore('some_semaphore', self.SEMAPHORE_INIT_COUNT)

    def test_acquire_nonblocking_once(self):
        res = self.semaphore.acquire()
        self.assertTrue(res)

    def test_acquire_nonblocking_four_times(self):
        for _ in range(4):
            res = self.semaphore.acquire()
        self.assertFalse(res)

    def test_acquire_blocking_once(self):
        res = self.semaphore.acquire(blocking=True)
        self.assertTrue(res)

    def test_release_acquired(self):
        self.semaphore.acquire()
        res = self.semaphore.release()
        self.assertEquals(self.SEMAPHORE_INIT_COUNT, res)

    def test_release_acquired_twice(self):
        self.semaphore.acquire()
        self.semaphore.acquire()
        res = self.semaphore.release()
        self.assertEquals(self.SEMAPHORE_INIT_COUNT - 1, res)

    def test_releasing_not_acquired(self):
        self.assertRaises(RuntimeError, self.semaphore.release)