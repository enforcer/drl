# -*- coding: utf-8 -*-
import os


REDIS_PORT = int(os.getenv('REDIS_PORT', '6379'))
REDIS_HOST = os.getenv('REDIS_HOST', '127.0.0.1')