# -*- coding: utf-8 -*-

import redis
import redis.exceptions
from drl import settings
_r_con = redis.StrictRedis(settings.REDIS_HOST, settings.REDIS_PORT)

try:
    _r_con.info()
except redis.exceptions.ConnectionError:
    print('Redis instance unreachable at {0}:{1}'.format(settings.REDIS_HOST, settings.REDIS_PORT))


class Mutex(object):
    UNLOCKED = 'unlocked'
    LOCKED = 'locked'

    def __init__(self, name):
        self._name = name
        _r_con.set(name, self.UNLOCKED)

    def acquire(self):
        _r_con.watch(self._name)
        state = _r_con.get(self._name)
        if state == self.LOCKED:
            _r_con.unwatch()
            raise RuntimeError
        else:
            if self._change_state_atomically(self.LOCKED) is not None:
                return True
            else:
                return False

    def release(self):
        state = _r_con.get(self._name)
        if state == self.UNLOCKED:
            raise RuntimeError
        else:
            if self._change_state_atomically(self.UNLOCKED) is None:
                return False
            else:
                return True

    def _change_state_atomically(self, state):
        assert state in (self.LOCKED, self.UNLOCKED)
        with _r_con.pipeline() as pipe:
            pipe.multi()
            pipe.set(self._name, state)
            return pipe.execute()


class Semaphore(object):

    def __init__(self, name, counter):
        self._name = name
        self._max_counter = counter
        _r_con.delete(name)
        values = (_r_con.randomkey() for _ in range(counter))
        for value in values:
            _r_con.lpush(self._name, value)

    def acquire(self, blocking=False):
        if not blocking:
            return _r_con.lpop(self._name) is not None
        else:
            return _r_con.blpop(self._name) is not None

    def release(self):
        _r_con.watch(self._name)
        counter = _r_con.llen(self._name)
        if counter < self._max_counter:
            with _r_con.pipeline() as pipe:
                pipe.multi()
                pipe.lpush(self._name, pipe.randomkey())
                pipe.llen(self._name)
                res = pipe.execute()
            if res is None:
                return False
            else:
                return res[1]  # llen result
        else:
            _r_con.unwatch()
            raise RuntimeError
