# README #


### Requirements ###
You need a redis instance up and running at localhost:6379. Settings can be adjusted - take a look at source.

### Example ###
```
#!python
import drl

mutex = drl.Mutex('some_mutex')
mutex.acquire()  # True
# mutex.acquire()  # RuntimeError
mutex.release()  # True
# mutex.release()  # RuntimeError

semaphore = drl.Semaphore('some_semaphore', 2)
semaphore.acquire(blocking=False)  # True
semaphore.acquire(blocking=False)  # True
semaphore.acquire(blocking=False)  # False
semaphore.release()  # 1
semaphore.release()  # 2
# semaphore.release()  # RuntimeError

# Blocking acquire
semaphore.acquire(blocking=True)  # True
semaphore.acquire(blocking=True)  # True
# semaphore.acquire(blocking=True)  # This would block until some other redis client releases the lock


```